package iotplatform.client.processor;

import iotplatform.client.mqtt.MqttSender;

public interface ICommandProcessor {
	/**
	 * Handle event when connection to MQTT broker is established
	 */
	public void onConnected();
	
	/**
	 * Handle event when receive system command (such as RegisterResponse)
	 * @param data
	 */
	public void onSystemCommand(byte[] data);
	
	/**
	 * Handle event when receive Specification Command (device-specific commands)
	 * @param data
	 */
	public void onSpecificationCommand(byte[] data);
	public void setHardwareId(String hardwareId);
	public void setSpecToken(String specToken);
	public void setSiteToken(String siteToken);
	public void setMqttSender(MqttSender sender);
	
	/**
	 * Disconnect from MQTT broker
	 */
	public void disconnect();
}
