package iotplatform.client.mqtt;

import org.fusesource.mqtt.client.BlockingConnection;
import org.fusesource.mqtt.client.Message;
import org.fusesource.mqtt.client.QoS;
import org.fusesource.mqtt.client.Topic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import iotplatform.client.processor.ICommandProcessor;

/**
 * Runnable thread for handling MQTT message from server
 * @author Pham Duy Anh
 *
 */
public class MqttReceiver implements Runnable {

	protected static Logger log = LoggerFactory.getLogger(MqttReceiver.class);

	/** MQTT connection */
	private BlockingConnection connection;

	/** System inbound MQTT topic */
	private String systemTopic;

	/** Command inbound MQTT topic */
	private String commandTopic;

	/** Register status */
	private boolean registerSuccess = false;

	private ICommandProcessor commandProcessor;

	public MqttReceiver(BlockingConnection connection, String systemTopic, String commandTopic,
			ICommandProcessor commandProcessor) {
		this.connection = connection;
		this.systemTopic = systemTopic;
		this.commandTopic = commandTopic;
		this.commandProcessor = commandProcessor;
	}

	private QoS systemTopicQos = QoS.AT_LEAST_ONCE;
	private QoS commandTopicQos = QoS.AT_LEAST_ONCE;

	public ICommandProcessor getCommandProcessor() {
		return commandProcessor;
	}

	public void setCommandProcessor(ICommandProcessor commandProcessor) {
		this.commandProcessor = commandProcessor;
	}

	public QoS getSystemTopicQos() {
		return systemTopicQos;
	}

	public void setSystemTopicQos(QoS systemTopicQos) {
		this.systemTopicQos = systemTopicQos;
	}

	public QoS getCommandTopicQos() {
		return commandTopicQos;
	}

	public void setCommandTopicQos(QoS commandTopicQos) {
		this.commandTopicQos = commandTopicQos;
	}

	@Override
	public void run() {
		// Subscribe to chosen topic.
		Topic[] topics =
			{
					new Topic(getSystemTopic(), systemTopicQos),
					new Topic(getCommandTopic(), commandTopicQos) 
			};
		try {
			log.info("Started MQTT receiver thread.");
			connection.subscribe(topics);
			log.info("Subscibed to topics {} and {}", getSystemTopic(), getCommandTopic());
			while (true) {
				try {
					Message message = connection.receive();
					log.info("Received message from topic: " + message.getTopic());
					message.ack();
					if (getSystemTopic().equals(message.getTopic())) {
						processSystemCommand(message.getPayload());
					} else if (getCommandTopic().equals(message.getTopic())) {
						processSpecificationCommand(message.getPayload());
					} else {
						log.warn("Message for unknown topic received: " + message.getTopic());
					}
				} catch (InterruptedException e) {
					log.warn("Inbound event processor interrupted.");
					return;
				} catch (Throwable e) {
					log.error("Exception processing inbound message", e);
				}
			}
		} catch (Exception e) {
			log.error("Exception while attempting to subscribe to inbound topics.", e);
		}
		System.out.println("SHUTDOWN MQTT RECEIVER THREAD");
	}

	protected void processSpecificationCommand(byte[] payload) {
		log.debug("Received device command:\n {}", new String(payload));
		try {

			/***********************************/
			// Execute the command logic in new thread 
			/***********************************/
			new Thread(new Runnable() {
				@Override
				public void run() {
					commandProcessor.onSpecificationCommand(payload);
				}
			}).start();


		} catch (Exception e) {
			log.error("Can not parse specification command", e);
		}
	}

	protected void processSystemCommand(byte[] payload) {
		log.debug("Receiving system command: {}", new String(payload));

		/* Execute logic in new thread to avoid locking receiver thread */
		new Thread(new Runnable() {

			@Override
			public void run() {
				commandProcessor.onSystemCommand(payload);
			}
		}).start();;

	}

	public BlockingConnection getConnection() {
		return connection;
	}

	public void setConnection(BlockingConnection connection) {
		this.connection = connection;
	}

	public String getSystemTopic() {
		return systemTopic;
	}

	public void setSystemTopic(String systemTopic) {
		this.systemTopic = systemTopic;
	}

	public String getCommandTopic() {
		return commandTopic;
	}

	public void setCommandTopic(String commandTopic) {
		this.commandTopic = commandTopic;
	}

	public boolean isRegisterSuccess() {
		return registerSuccess;
	}

	public void setRegisterSuccess(boolean registerSuccess) {
		this.registerSuccess = registerSuccess;
	}

}

