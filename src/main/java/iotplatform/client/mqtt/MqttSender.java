package iotplatform.client.mqtt;

import java.util.Date;
import java.util.Map;

import org.fusesource.mqtt.client.BlockingConnection;
import org.fusesource.mqtt.client.QoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import iotplatform.client.json.JsonUtil;
import iotplatform.json.model.event.Alert;
import iotplatform.json.model.event.Location;
import iotplatform.json.model.event.Measurement;
import iotplatform.json.model.message.DeviceCommandResponse;
import iotplatform.json.model.message.EventRequest;
import iotplatform.json.model.message.RegistrationRequest;
import iotplatform.json.model.message.EventRequest.RequestType;

/**
 * Class for sending outbound messages to server
 * @author Pham Duy Anh
 *
 */
public class MqttSender {
	protected static Logger log = LoggerFactory.getLogger(MqttSender.class);
	
	protected QoS defaultQos = QoS.AT_MOST_ONCE;
	
	/** MQTT outbound topic */
	protected String topic;

	/** MQTT connection */
	protected BlockingConnection connection;

	protected String hardwareId;		// device Id
	protected String siteToken;
	protected String specToken;

	
//	public MqttSender(BlockingConnection connection, String eventDeliveryTopic, String hardwareId, String siteToken,
//			String specToken) {
//		this.hardwareId = hardwareId;
//		this.connection = connection;
//		this.topic = eventDeliveryTopic;
//		this.siteToken = siteToken;
//		this.specToken = specToken;
//	}

	public QoS getDefaultQos() {
		return defaultQos;
	}

	public void setDefaultQos(QoS defaultQos) {
		this.defaultQos = defaultQos;
	}

	public String getHardwareId() {
		return hardwareId;
	}

	public void setHardwareId(String hardwareId) {
		this.hardwareId = hardwareId;
	}

	public String getSiteToken() {
		return siteToken;
	}

	public void setSiteToken(String siteToken) {
		this.siteToken = siteToken;
	}

	public String getSpecToken() {
		return specToken;
	}

	public void setSpecToken(String specToken) {
		this.specToken = specToken;
	}

	public BlockingConnection getConnection() {
		return connection;
	}

	public void setConnection(BlockingConnection connection) {
		this.connection = connection;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}
	
}
