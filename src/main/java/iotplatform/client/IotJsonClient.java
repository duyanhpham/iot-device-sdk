package iotplatform.client;

import org.fusesource.mqtt.client.BlockingConnection;
import iotplatform.client.json.JsonUtil;
import iotplatform.client.json.MqttSenderJson;
import iotplatform.client.mqtt.MqttReceiver;
import iotplatform.client.mqtt.MqttSender;

public class IotJsonClient extends IotAbstractClient {

	public void start() {
		if (eventDeliveryTopic==null) {
			eventDeliveryTopic = tenantId + "/input/json";
		}
		if (systemCommandTopic==null) {
			systemCommandTopic = tenantId + "/system/";
		}
		if (deviceCommandTopic==null) {
			deviceCommandTopic = tenantId + "/commands/";
		}

		BlockingConnection connection = connectMqttServer();

		JsonUtil.setDefaultDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

		log.info("Create MQTT Sender to send message to server");

		MqttSender sender;

		sender = new MqttSenderJson(connection, eventDeliveryTopic, hardwareId, siteToken, specToken);
		sender.setDefaultQos(mqttSenderQos);

		log.info("Initialize Command Processor");
		if (commandProcessor == null) {
			log.error("No command processor found! Nothing to do");
			throw new RuntimeException("CommandProcessor can not be null");
		}
		commandProcessor.setHardwareId(hardwareId);
		commandProcessor.setSiteToken(siteToken);
		commandProcessor.setSpecToken(specToken);
		commandProcessor.setMqttSender(sender);

		if (!systemCommandTopic.endsWith("/"))
			systemCommandTopic += "/";
		if (!deviceCommandTopic.endsWith("/"))
			deviceCommandTopic += "/";
		log.info("Create MQTT Receiver to receive subscribed message in a new thread");
		MqttReceiver receiver = new MqttReceiver(connection,
				systemCommandTopic + hardwareId, 
				deviceCommandTopic + hardwareId, 
				commandProcessor);
		receiver.setCommandTopicQos(commandTopicQos);
		receiver.setSystemTopicQos(systemTopicQos);
		new Thread(receiver).start();

		try {
			Thread.sleep(1000l);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}	// wait a little bit for MQTT connection to be established

		commandProcessor.onConnected();

		// Handle shutdown gracefully.
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("Receive kill signal");
				if (connection != null) {
					try {
						connection.disconnect();
						log.info("Disconnected from MQTT broker.");
					} catch (Exception e) {
						log.error("Exception disconnecting from MQTT broker.", e);
					}
				}
			}
		}));
	}


}
