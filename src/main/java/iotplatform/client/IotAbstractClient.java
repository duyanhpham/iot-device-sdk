package iotplatform.client;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.fusesource.mqtt.client.BlockingConnection;
import org.fusesource.mqtt.client.MQTT;
import org.fusesource.mqtt.client.QoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import iotplatform.client.json.JsonUtil;
import iotplatform.client.json.MqttSenderJson;
import iotplatform.client.mqtt.MqttReceiver;
import iotplatform.client.mqtt.MqttSender;
import iotplatform.client.processor.ICommandProcessor;
import iotplatform.client.protobuf.MqttSenderProtobuf;

public class IotAbstractClient {

	protected String tenantId;

	protected String eventDeliveryTopic;
	protected String deviceCommandTopic;
	protected String systemCommandTopic;

	protected QoS systemTopicQos = QoS.AT_LEAST_ONCE;
	protected QoS commandTopicQos = QoS.AT_LEAST_ONCE;
	protected QoS mqttSenderQos = QoS.AT_MOST_ONCE;

	static Logger log = LoggerFactory.getLogger(IotAbstractClient.class);

	String mqttServerUrl;
	String mqttUsername;
	String mqttPassword;
	String hardwareId;
	String siteToken = null;
	String specToken;

	String serverCertificateFile;
	boolean acceptAllCertificate = false;

	protected ICommandProcessor commandProcessor;

	public String getTenantId() {
		return tenantId;
	}


	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}


	public QoS getSystemTopicQos() {
		return systemTopicQos;
	}


	public void setSystemTopicQos(QoS systemTopicQos) {
		this.systemTopicQos = systemTopicQos;
	}


	public QoS getCommandTopicQos() {
		return commandTopicQos;
	}


	public void setCommandTopicQos(QoS commandTopicQos) {
		this.commandTopicQos = commandTopicQos;
	}


	public QoS getMqttSenderQos() {
		return mqttSenderQos;
	}


	public void setMqttSenderQos(QoS mqttSenderQos) {
		this.mqttSenderQos = mqttSenderQos;
	}


	public String getEventDeliveryTopic() {
		return eventDeliveryTopic;
	}


	public void setEventDeliveryTopic(String eventDeliveryTopic) {
		this.eventDeliveryTopic = eventDeliveryTopic;
	}


	public String getDeviceCommandTopic() {
		return deviceCommandTopic;
	}


	public void setDeviceCommandTopic(String deviceCommandTopic) {
		this.deviceCommandTopic = deviceCommandTopic;
	}


	public String getSystemCommandTopic() {
		return systemCommandTopic;
	}


	public void setSystemCommandTopic(String systemCommandTopic) {
		this.systemCommandTopic = systemCommandTopic;
	}

	public ICommandProcessor getCommandProcessor() {
		return commandProcessor;
	}

	public String getServerCertificateFile() {
		return serverCertificateFile;
	}


	public void setServerCertificateFile(String serverCertificateFile) {
		this.serverCertificateFile = serverCertificateFile;
	}


	public boolean isAcceptAllCertificate() {
		return acceptAllCertificate;
	}


	public void setAcceptAllCertificate(boolean acceptAllCertificate) {
		this.acceptAllCertificate = acceptAllCertificate;
	}


	public IotAbstractClient() {

	}


	public String getMqttServerUrl() {
		return mqttServerUrl;
	}


	public void setMqttServerUrl(String mqttServerUrl) {
		this.mqttServerUrl = mqttServerUrl;
	}


	public String getMqttUsername() {
		return mqttUsername;
	}


	public void setMqttUsername(String mqttUsername) {
		this.mqttUsername = mqttUsername;
	}


	public String getMqttPassword() {
		return mqttPassword;
	}


	public void setMqttPassword(String mqttPassword) {
		this.mqttPassword = mqttPassword;
	}


	public String getHardwareId() {
		return hardwareId;
	}


	public void setHardwareId(String hardwareId) {
		this.hardwareId = hardwareId;
	}


	public String getSiteToken() {
		return siteToken;
	}


	public void setSiteToken(String siteToken) {
		this.siteToken = siteToken;
	}


	public String getSpecToken() {
		return specToken;
	}


	public void setSpecToken(String specToken) {
		this.specToken = specToken;
	}

	public void setCommandProcessor(ICommandProcessor cmd) {
		this.commandProcessor = cmd;
	}

	public void stop() {
		getCommandProcessor().disconnect();
	}
	
	public BlockingConnection connectMqttServer() {
		log.info("Connecting to MQTT broker " + mqttServerUrl );
		MQTT mqtt = new MQTT();
		try {
			mqtt.setHost(mqttServerUrl);
			if (mqttUsername!=null) mqtt.setUserName(mqttUsername);
			if (mqttPassword!=null) mqtt.setPassword(mqttPassword);
		} catch (URISyntaxException e) {
			log.error("Invalid MQTT broker URL");
			throw new RuntimeException("Invalid MQTT broker URL");
		}

		if (mqttServerUrl.startsWith("ssl") || mqttServerUrl.startsWith("tls")) {
			// Create a trust manager that does not validate certificate chains
			if (acceptAllCertificate || serverCertificateFile==null || serverCertificateFile.isEmpty()) {
				TrustManager[] trustAllCerts = new TrustManager[] { 
						new X509TrustManager() {     
							public java.security.cert.X509Certificate[] getAcceptedIssuers() { 
								return new X509Certificate[0];
							} 
							public void checkClientTrusted( 
									java.security.cert.X509Certificate[] certs, String authType) {
							} 
							public void checkServerTrusted( 
									java.security.cert.X509Certificate[] certs, String authType) {
							}
						} 
				}; 

				// Install the all-trusting trust manager
				try {
					SSLContext sc = SSLContext.getInstance("TLS"); 
					sc.init(null, trustAllCerts, new java.security.SecureRandom()); 
					mqtt.setSslContext(sc);

					//HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
				} catch (GeneralSecurityException e) {
					throw new RuntimeException("Unable to establish SSL connection", e);
				}

			} else if (serverCertificateFile!=null) {

				try {
					mqtt.setSslContext(buildSSLContext(serverCertificateFile));
				} catch (Exception e) {
					log.error("SSL Error");
					throw new RuntimeException("Unable to establish SSL connection", e);
				}
			} else {
				log.error("SSL Error");
				throw new RuntimeException("A server certificate file must be provided when using SSL/TLS");
			}
		}

		BlockingConnection connection = mqtt.blockingConnection();
		try {
			connection.connect();
		} catch (Exception e) {
			log.error("Unable to establish MQTT connection");
			throw new RuntimeException("Unable to establish MQTT connection");
		}
		log.info("Connected to MQTT broker.");
		return connection;
	}
	
	protected static SSLContext buildSSLContext(String serverCertFile) throws Exception {
		// Add support for self-signed (local) SSL certificates
		// Based on http://developer.android.com/training/articles/security-ssl.html#UnknownCa

		// Load CAs from an InputStream
		// (could be from a resource or ByteArrayInputStream or ...)
		CertificateFactory cf = CertificateFactory.getInstance("X.509");
		// From https://www.washington.edu/itconnect/security/ca/load-der.crt
		InputStream is = new FileInputStream(new File(serverCertFile));
		InputStream caInput = new BufferedInputStream(is);
		Certificate ca;
		try {
			ca = cf.generateCertificate(caInput);
			// System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
		} finally {
			caInput.close();
		}

		// Create a KeyStore containing our trusted CAs
		String keyStoreType = KeyStore.getDefaultType();
		KeyStore keyStore = KeyStore.getInstance(keyStoreType);
		keyStore.load(null, null);
		keyStore.setCertificateEntry("ca", ca);

		// Create a TrustManager that trusts the CAs in our KeyStore
		String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
		TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
		tmf.init(keyStore);

		// Create an SSLContext that uses our TrustManager
		SSLContext context = SSLContext.getInstance("TLS");
		context.init(null, tmf.getTrustManagers(), null);
		return context;
	}
	
	
}
