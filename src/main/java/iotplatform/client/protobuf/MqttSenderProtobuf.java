package iotplatform.client.protobuf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.fusesource.mqtt.client.BlockingConnection;
import org.fusesource.mqtt.client.QoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.protobuf.AbstractMessageLite;

import iotplatform.client.mqtt.MqttSender;
import iotplatform.exception.IotException;
import iotplatform.json.model.message.DeviceCommandResponse;
import iotplatform.json.model.message.EventRequest;
import iotplatform.json.model.message.EventRequest.RequestType;
import iotplatform.protobuf.proto.Iot.Model;
import iotplatform.protobuf.proto.Iot.IotEvent;
import iotplatform.protobuf.proto.Iot.IotEvent.Command;
import iotplatform.protobuf.proto.Iot.IotEvent.RegisterDevice;

/**
 * Class for sending outbound messages to server
 * @author Pham Duy Anh
 *
 */
public class MqttSenderProtobuf extends MqttSender {

	protected static Logger log = LoggerFactory.getLogger(MqttSenderProtobuf.class);
	
	public MqttSenderProtobuf(BlockingConnection connection, String topic, String hardwareId, String siteToken,
			String specToken) {
		this.connection = connection;
		this.topic = topic;
		this.hardwareId = hardwareId;
		this.siteToken = siteToken;
		this.specToken = specToken;
	}
	

	public void sendRegistration() throws IotException {
		sendRegistration(QoS.EXACTLY_ONCE);
	}
	
	public void sendRegistration(QoS qos) throws IotException {
		log.info("Send register to server");
		RegisterDevice.Builder builder = RegisterDevice.newBuilder();
		builder = builder.setHardwareId(hardwareId).setSpecificationToken(specToken);
		if (siteToken != null && !siteToken.isEmpty())
			builder = builder.setSiteToken(siteToken);
		RegisterDevice register = builder.build();
		
		sendMessage(Command.SEND_REGISTRATION, register, qos);
	}

	public void sendCommandResponse(String commandInvocationId, String response) {
		sendCommandResponse(commandInvocationId, response, defaultQos);
	}
	public void sendCommandResponse(String commandInvocationId, String response, QoS qos) {
		IotEvent.Acknowledge.Builder ack = IotEvent.Acknowledge.newBuilder();
		ack.setHardwareId(hardwareId);
		ack.setMessage(response);
		sendMessage(Command.SEND_ACKNOWLEDGEMENT, commandInvocationId, ack.build(), qos);
	}

	public void sendMeasurement(String name, double value) throws IotException {
		sendMeasurement(name, value, defaultQos);
	}
	
	public void sendMeasurement(String name, double value, QoS qos) throws IotException {
		Model.DeviceMeasurements.Builder mb = Model.DeviceMeasurements.newBuilder();
		mb.setHardwareId(hardwareId).addMeasurement(
				Model.Measurement.newBuilder().setMeasurementId(name).setMeasurementValue(value).build());
		mb.setEventDate(System.currentTimeMillis());
		sendMessage(Command.SEND_DEVICE_MEASUREMENTS, mb.build(), qos);
	}
	
	public void sendMeasurement(Map<String, Double> measurements) throws IotException {
		sendMeasurement(measurements, true, defaultQos);
	}
	
	public void sendMeasurement(Map<String, Double> measurements, boolean updateState) throws IotException {
		sendMeasurement(measurements, updateState, defaultQos);
	}
	public void sendMeasurement(Map<String, Double> measurements, boolean updateState, QoS qos) throws IotException {
		Model.DeviceMeasurements.Builder mb = Model.DeviceMeasurements.newBuilder();
		mb.setHardwareId(hardwareId);
		Iterator it = measurements.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, Double> e = (Entry<String, Double>) it.next();
			mb.addMeasurement(
				Model.Measurement.newBuilder().setMeasurementId(e.getKey()).setMeasurementValue(e.getValue()).build());
		}
		mb.setEventDate(System.currentTimeMillis());
		sendMessage(Command.SEND_DEVICE_MEASUREMENTS, mb.build(), qos);
	}

	public void sendLocation(double latitude, double longtitude, double elevation) throws IotException {
		sendLocation(latitude, longtitude, elevation, defaultQos);
	}
	public void sendLocation(double latitude, double longtitude, double elevation, QoS qos) throws IotException {
		Model.DeviceLocation.Builder lb = Model.DeviceLocation.newBuilder();
		lb.setHardwareId(hardwareId);
		lb.setEventDate(System.currentTimeMillis());
		lb.setLatitude(latitude);
		lb.setLongitude(longtitude);
		lb.setElevation(elevation);
		sendMessage(Command.SEND_DEVICE_LOCATION, lb.build(), qos);
	}
	
	public void sendAlert(String alertType, Model.AlertLevel level, String message,  Map<String, String> metadata) throws IotException {
		sendAlert(alertType, level, message, metadata, defaultQos);
	}
	
	public void sendAlert(String alertType, Model.AlertLevel level, String message, Map<String, String> metadata, QoS qos) throws IotException {
		Model.DeviceAlert.Builder ab = Model.DeviceAlert.newBuilder();
		ab.setAlertMessage(message);
		ab.setAlertType(alertType);
		ab.setAlertLevel(level);
		ab.setEventDate(System.currentTimeMillis());
		ab.setHardwareId(hardwareId);
		if (metadata!=null) {
			for (Map.Entry<String, String> entry : metadata.entrySet())
			{
				ab.addMetadata(Model.Metadata.newBuilder().setName(entry.getKey())
						.setValue(entry.getValue()).build());
			}
		}
		sendMessage(Command.SEND_DEVICE_ALERT, ab.build(), defaultQos);
	}
	
	/**
	 * Common logic for sending messages via protocol buffers.
	 */
	protected void sendMessage(IotEvent.Command command, AbstractMessageLite message, QoS qos) throws IotException {
		sendMessage(command, null, message, qos);
	}
	
	/**
	 * Common logic for sending messages via protocol buffers.
	 */
	protected void sendMessage(IotEvent.Command command, String originator, AbstractMessageLite message, QoS qos) throws IotException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			IotEvent.Header.Builder builder = IotEvent.Header.newBuilder();
			builder.setCommand(command);
			if (originator!=null) 
				builder.setOriginator(originator);
			builder.build().writeDelimitedTo(out);
			message.writeDelimitedTo(out);
			log.debug("Publishing " + out.toByteArray().length + " bytes to " + getTopic());
			
			connection.publish(getTopic(), out.toByteArray(), qos, false);
			
		} catch (IOException e) {
			throw new IotException("Problem encoding " + command + " message.", e);
		} catch (Exception e) {
			throw new IotException(e);
		}
	}

}
