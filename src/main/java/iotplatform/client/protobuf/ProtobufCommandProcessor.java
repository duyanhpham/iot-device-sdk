package iotplatform.client.protobuf;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.fusesource.mqtt.client.QoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import iotplatform.client.mqtt.MqttSender;
import iotplatform.client.processor.ICommandProcessor;
import iotplatform.protobuf.proto.Iot.Device;
import iotplatform.protobuf.proto.Iot.Device.Header;
import iotplatform.protobuf.proto.Iot.Device.RegistrationAck;
import iotplatform.protobuf.proto.Iot.Device.RegistrationAckState;

public abstract class ProtobufCommandProcessor implements ICommandProcessor {
	
	protected static Logger log = LoggerFactory.getLogger(ProtobufCommandProcessor.class);
	
	String hardwareId;
	String siteToken;
	String specToken;
	
	MqttSenderProtobuf mqttSender;
	
	public abstract void onReceiveDeviceCommand(byte[] data);
	public abstract void onRegistrationSuccess(RegistrationAck ack);
	public abstract void onRegistrationFailure(RegistrationAck ack);
	
	@Override
	public void setMqttSender(MqttSender mqttSender) {
		this.mqttSender = (MqttSenderProtobuf) mqttSender;
	}
	public MqttSenderProtobuf getMqttSender() {
		return mqttSender;
	}
	public String getHardwareId() {
		return hardwareId;
	}
	public void setHardwareId(String hardwareId) {
		this.hardwareId = hardwareId;
	}
	public String getSiteToken() {
		return siteToken;
	}
	public void setSiteToken(String siteToken) {
		this.siteToken = siteToken;
	}
	public String getSpecToken() {
		return specToken;
	}
	public void setSpecToken(String specToken) {
		this.specToken = specToken;
	}

	@Override
	public void onConnected() {
		try {
			getMqttSender().sendRegistration(QoS.EXACTLY_ONCE);
		} catch (Exception e) {
			log.error("Exception when send registration message", e);;
		}
	}
	
	@Override
	public void onSystemCommand(byte[] data) {
		ByteArrayInputStream stream = new ByteArrayInputStream(data);
		try {
			Header header = Device.Header.parseDelimitedFrom(stream);
			switch (header.getCommand()) {
			case ACK_REGISTRATION: {
				RegistrationAck ack = RegistrationAck.parseDelimitedFrom(stream);
				log.debug("Receiving RegistrationAck: {}", ack);
				if (ack.getState() == RegistrationAckState.REGISTRATION_ERROR) {
					log.debug("Registration failed: " + ack.getErrorMessage());
					onRegistrationFailure(ack);
				} else {
					log.info("Registration successful");
					onRegistrationSuccess(ack);
				}
				break;
			}
			case ACK_DEVICE_STREAM: {
				// TODO: Add device stream support.
				break;
			}
			case RECEIVE_DEVICE_STREAM_DATA: {
				// TODO: Add device stream support.
				break;
			}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	@Override
	public void onSpecificationCommand(byte[] data) {
		onReceiveDeviceCommand(data);
	}

	@Override
	public void disconnect() {
		try {
			log.info("Disconnect from MQTT broker");
			getMqttSender().getConnection().disconnect();
		} catch (Exception e) {
			log.error("Exception", e);
		}
	}
	
}
