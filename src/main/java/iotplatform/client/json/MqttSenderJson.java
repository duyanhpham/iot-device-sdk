package iotplatform.client.json;

import java.util.Date;
import java.util.Map;

import org.fusesource.mqtt.client.BlockingConnection;
import org.fusesource.mqtt.client.QoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import iotplatform.client.mqtt.MqttSender;
import iotplatform.json.model.event.Alert;
import iotplatform.json.model.event.AlertLevel;
import iotplatform.json.model.event.Location;
import iotplatform.json.model.event.Measurement;
import iotplatform.json.model.message.DeviceCommandResponse;
import iotplatform.json.model.message.EventRequest;
import iotplatform.json.model.message.RegistrationRequest;
import iotplatform.json.model.message.EventRequest.RequestType;

/**
 * Class for sending outbound messages to server
 * @author Pham Duy Anh
 *
 */
public class MqttSenderJson extends MqttSender {
	protected static Logger log = LoggerFactory.getLogger(MqttSenderJson.class);
	
	public MqttSenderJson(BlockingConnection connection, String topic, String hardwareId, String siteToken, String specToken) {
		this.connection = connection;
		this.topic = topic;
		this.hardwareId = hardwareId;
		this.siteToken = siteToken;
		this.specToken = specToken;
	}

	public void sendRegistration() {
		sendRegistration(QoS.EXACTLY_ONCE);
	}
	
	public void sendRegistration(QoS qos) {
		log.info("Send register to server");
		EventRequest req = new EventRequest(hardwareId, RequestType.RegisterDevice, 
				new RegistrationRequest(hardwareId, specToken, siteToken));
		sendMessage(req, qos);
	}

	public void sendCommandResponse(String commandInvocationId, String response) {
		sendCommandResponse(commandInvocationId, response, defaultQos);
	}
	public void sendCommandResponse(String commandInvocationId, String response, QoS qos) {
		EventRequest req = new EventRequest(hardwareId, RequestType.Acknowledge, 
				new DeviceCommandResponse(response, new Date(), commandInvocationId, false));
		sendMessage(req, qos);
	}

	public void sendMeasurement(Map<String, Double> measurements, boolean updateState) {
		sendMeasurement(measurements, updateState, defaultQos);
	}
	public void sendMeasurement(Map<String, Double> measurements, boolean updateState, QoS qos) {
		EventRequest req = new EventRequest(hardwareId, RequestType.DeviceMeasurements, 
				new Measurement(measurements, updateState, new Date()));
		sendMessage(req, qos);
	}

	public void sendLocation(double latitude, double longtitude, double elevation, boolean updateState) {
		sendLocation(latitude, longtitude, elevation, updateState, defaultQos);
	}
	public void sendLocation(double latitude, double longtitude, double elevation, boolean updateState, QoS qos) {
		EventRequest req = new EventRequest(hardwareId, RequestType.DeviceLocation, 
				new Location(latitude, longtitude, elevation, updateState, new Date()));
		sendMessage(req, qos);
	}

	public void sendLocation(Location location) {
		sendLocation(location, defaultQos);
	}
	public void sendLocation(Location location, QoS qos) {
		EventRequest req = new EventRequest(hardwareId, RequestType.DeviceLocation, location);
		sendMessage(req, qos);
	}

	public void sendAlert(String alertType, AlertLevel level, String message, boolean updateState, Map<String, String> metadata) {
		sendAlert(alertType, level, message, updateState, metadata, defaultQos);
	}
	
	public void sendAlert(String alertType, AlertLevel level, String message, boolean updateState, Map<String, String> metadata, QoS qos) {
		EventRequest req = new EventRequest(hardwareId, RequestType.DeviceAlert, 
				new Alert(alertType, level, message, updateState, new Date(), metadata));
		sendMessage(req, qos);
	}
	
	public void sendAlert(Alert alert) {
		sendAlert(alert, defaultQos);
	}
	public void sendAlert(Alert alert, QoS qos) {
		EventRequest req = new EventRequest(hardwareId, RequestType.DeviceAlert, alert);
		sendMessage(req, qos);
	}

	public void sendMessage(EventRequest request) {
		sendMessage(request, defaultQos);
	}
	
	public void sendMessage(EventRequest request, QoS qos) {
        try {
        	log.debug("Publishing message to MQTT topic " + getTopic() + " /n " + JsonUtil.object2string(request));
			connection.publish(getTopic(), JsonUtil.object2string(request).getBytes(), 
					qos, false);
		} catch (Exception e) {
			log.error("Publish message error");
		}
	}
}
