package iotplatform.client.json;

import java.io.IOException;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * JSON Utility class using the Jackson library
 * (Jackson is more powerful and has more feature than Gson)
 * 
 * @author Pham Duy Anh
 *
 */
public class JsonUtil {

	private static final ObjectMapper mapper = new ObjectMapper();
	private static boolean prettyPrint = false;

	static {
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
	}
	
	public static void setDefaultDateFormat(String pattern) {
		mapper.setDateFormat(new SimpleDateFormat(pattern));
	}
	
	public static void setPrettyPrint(boolean enablePrettyPrint) {
		prettyPrint = enablePrettyPrint;
	}
	/**
	 * Parse a JSON String into a corresponding object
	 * return null if parse fail
	 * @param jsonString
	 * @param className
	 * @return
	 */
	public static Object string2object(String jsonString, Class<?> className) {
		try {
			return mapper.readValue(jsonString, className);
		} catch (JsonParseException e) {
			e.printStackTrace();
			return null;
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	/**
	 * Create a JSON String from a given object
	 * return null if error
	 * @param obj
	 * @return
	 */
	public static String object2string(Object obj) {
		try {
			if (!prettyPrint)
				return mapper.writeValueAsString(obj);
			else
				return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			return null;
		}
	}
	
	/**
	 * Create a JSON String from a given object
	 * Result string is formated for easier viewing 
	 * @param obj
	 * @return
	 */
	public static String object2stringPretty(Object obj) {
		try {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			return null;
		}
	}
}
