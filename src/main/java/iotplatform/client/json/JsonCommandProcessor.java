package iotplatform.client.json;

import org.fusesource.mqtt.client.QoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import iotplatform.client.mqtt.MqttSender;
import iotplatform.client.processor.ICommandProcessor;
import iotplatform.json.model.message.DeviceCommandRequest;
import iotplatform.json.model.message.RegistrationResponse;
import iotplatform.json.model.message.SystemCommand.SystemCommandType;

public abstract class JsonCommandProcessor implements ICommandProcessor {
	protected static Logger log = LoggerFactory.getLogger(JsonCommandProcessor.class);
	
	String hardwareId;
	String siteToken;
	String specToken;
	
	MqttSenderJson mqttSender;
	
	public abstract void onReceiveDeviceCommand(DeviceCommandRequest command);
	public abstract void onRegistrationSuccess(RegistrationResponse response);
	public abstract void onRegistrationFailure(RegistrationResponse response);
	
	@Override
	public void setMqttSender(MqttSender mqttSender) {
		this.mqttSender = (MqttSenderJson) mqttSender;
	}
	
	public MqttSenderJson getMqttSender() {
		return mqttSender;
	}
	public String getHardwareId() {
		return hardwareId;
	}
	public void setHardwareId(String hardwareId) {
		this.hardwareId = hardwareId;
	}
	public String getSiteToken() {
		return siteToken;
	}
	public void setSiteToken(String siteToken) {
		this.siteToken = siteToken;
	}
	public String getSpecToken() {
		return specToken;
	}
	public void setSpecToken(String specToken) {
		this.specToken = specToken;
	}

	@Override
	public void onConnected() {
		getMqttSender().sendRegistration(QoS.EXACTLY_ONCE);
	}
	
	@Override
	public void onSystemCommand(byte[] data) {
		try {
			RegistrationResponse resp = (RegistrationResponse) JsonUtil.string2object(new String(data), RegistrationResponse.class);
			log.debug("Receive register response: " + resp);

			if (resp == null)
				throw new RuntimeException("Can not parse system command to RegistrationResponse");

			if (resp.getSystemCommand().getType().equals(SystemCommandType.RegistrationAck)) {
				log.info("Register successfully");
				onRegistrationSuccess(resp);

			} else if (resp.getSystemCommand().getType().equals(SystemCommandType.RegistrationFailure)) {
				log.error("Register device failed");
				onRegistrationFailure(resp);
			} else {
				log.debug("Unknown system command");
			}
		} catch (Exception e) {
			log.error("Error parsing system command", e);
		}		
	}
	@Override
	public void onSpecificationCommand(byte[] data) {
		try {
			DeviceCommandRequest cmd = (DeviceCommandRequest) JsonUtil.string2object(new String(data), DeviceCommandRequest.class);
			log.debug("Parsed device command: {}", cmd.toString());

			/***********************************/
			// Execute the command logic in new thread 
			/***********************************/
			new Thread(new Runnable() {
				@Override
				public void run() {
					onReceiveDeviceCommand(cmd);
				}
			}).start();


		} catch (Exception e) {
			log.error("Can not parse device command", e);
		}		
	}

	@Override
	public void disconnect() {
		try {
			log.info("Disconnect from MQTT broker");
			getMqttSender().getConnection().disconnect();
		} catch (Exception e) {
			log.error("Exception", e);
		}
	}
	
}
