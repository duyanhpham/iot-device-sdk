package iotplatform.exception;

import java.io.IOException;

public class IotException extends RuntimeException {

	public IotException(String message, Exception e) {
		super(message, e);
	}
	
	public IotException(String message) {
		super(message);
	}
	
	public IotException(Exception e) {
		super(e);
	}

}
