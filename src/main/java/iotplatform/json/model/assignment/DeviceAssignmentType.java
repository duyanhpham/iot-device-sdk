package iotplatform.json.model.assignment;

/**
 * Indicates what type of asset is linked via the assignment.
 */
public enum DeviceAssignmentType {

	/** No associated asset */
	Unassociated,

	/** Assignment is associated with an asset */
	Associated;
}