/*
 * Copyright (c) SiteWhere, LLC. All rights reserved. http://www.sitewhere.com
 *
 * The software in this package is published under the terms of the CPAL v1.0
 * license, a copy of which has been included with this distribution in the
 * LICENSE.txt file.
 */
package iotplatform.json.model.assignment;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Device assignment value object used for marshaling.
 */
@JsonInclude(Include.NON_NULL)
public class DeviceAssignment  implements Serializable {

	/** Unique assignment token */
	private String token;

	/** Device hardware id */
	private String deviceHardwareId;

	/** Type of associated asset */
	private DeviceAssignmentType assignmentType;

	/** Id of asset module */
	private String assetModuleId;

	/** Id of associated asset */
	private String assetId;

	/** Site token */
	private String siteToken;

	/** Assignment status */
	private DeviceAssignmentStatus status;

	/** Assignment start date */
	private Date activeDate;

	/** Assignment end date */
	private Date releasedDate;

	Map<String, String> metadata;
	
	private String createdDate;
    private Date updatedDate;
    private String createdBy;
    private String updatedBy;
    
    private boolean deleted;
    
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getDeviceHardwareId() {
		return deviceHardwareId;
	}
	public void setDeviceHardwareId(String deviceHardwareId) {
		this.deviceHardwareId = deviceHardwareId;
	}
	public DeviceAssignmentType getAssignmentType() {
		return assignmentType;
	}
	public void setAssignmentType(DeviceAssignmentType assignmentType) {
		this.assignmentType = assignmentType;
	}
	public String getAssetModuleId() {
		return assetModuleId;
	}
	public void setAssetModuleId(String assetModuleId) {
		this.assetModuleId = assetModuleId;
	}
	public String getAssetId() {
		return assetId;
	}
	public void setAssetId(String assetId) {
		this.assetId = assetId;
	}
	public String getSiteToken() {
		return siteToken;
	}
	public void setSiteToken(String siteToken) {
		this.siteToken = siteToken;
	}
	public DeviceAssignmentStatus getStatus() {
		return status;
	}
	public void setStatus(DeviceAssignmentStatus status) {
		this.status = status;
	}
	public Date getActiveDate() {
		return activeDate;
	}
	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}
	public Date getReleasedDate() {
		return releasedDate;
	}
	public void setReleasedDate(Date releasedDate) {
		this.releasedDate = releasedDate;
	}
	public Map<String, String> getMetadata() {
		return metadata;
	}
	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
}