package iotplatform.json.model.command;

import java.util.Map;

public class DeviceCommandExecution {
	DeviceCommand command;
	DeviceCommandInvocation invocation;
	Map<String, Object> parameters;
	
	public DeviceCommand getCommand() {
		return command;
	}
	public void setCommand(DeviceCommand command) {
		this.command = command;
	}
	public DeviceCommandInvocation getInvocation() {
		return invocation;
	}
	public void setInvocation(DeviceCommandInvocation invocation) {
		this.invocation = invocation;
	}
	public Map<String, Object> getParameters() {
		return parameters;
	}
	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DeviceCommand [command=");
		builder.append(command);
		builder.append(", invocation=");
		builder.append(invocation);
		builder.append(", parameters=");
		builder.append(parameters);
		builder.append("]");
		return builder.toString();
	}
	
	
}
