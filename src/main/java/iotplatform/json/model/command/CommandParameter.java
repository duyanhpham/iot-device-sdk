package iotplatform.json.model.command;

public class CommandParameter {
	String name;
	String type;
	boolean required;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public boolean isRequired() {
		return required;
	}
	public void setRequired(boolean required) {
		this.required = required;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Parameters [name=");
		builder.append(name);
		builder.append(", type=");
		builder.append(type);
		builder.append(", required=");
		builder.append(required);
		builder.append("]");
		return builder.toString();
	}
	
	
}
