package iotplatform.json.model.command;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class DeviceCommand {

    private String token;

    private String description;

    private String specificationToken;

    private Date updatedDate;

    private String name;

    private List<CommandParameter> parameters;

    private String deleted;

    private String createdDate;

    private Map<String, String> metadata;

    private String namespace;

    private String createdBy;
    
    private String updatedBy;

    public String getCreatedBy ()
    {
        return createdBy;
    }

    public void setCreatedBy (String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getToken ()
    {
        return token;
    }

    public void setToken (String token)
    {
        this.token = token;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getSpecificationToken ()
    {
        return specificationToken;
    }

    public void setSpecificationToken (String specificationToken)
    {
        this.specificationToken = specificationToken;
    }

    public Date getUpdatedDate ()
    {
        return updatedDate;
    }

    public void setUpdatedDate (Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public List<CommandParameter> getParameters ()
    {
        return parameters;
    }

    public void setParameters (List<CommandParameter> parameters)
    {
        this.parameters = parameters;
    }

    public String getDeleted ()
    {
        return deleted;
    }

    public void setDeleted (String deleted)
    {
        this.deleted = deleted;
    }

    public String getCreatedDate ()
    {
        return createdDate;
    }

    public void setCreatedDate (String createdDate)
    {
        this.createdDate = createdDate;
    }

    public Map<String, String> getMetadata ()
    {
        return metadata;
    }

    public void setMetadata ( Map<String, String> metadata)
    {
        this.metadata = metadata;
    }

    public String getNamespace ()
    {
        return namespace;
    }

    public void setNamespace (String namespace)
    {
        this.namespace = namespace;
    }

    public String getUpdatedBy ()
    {
        return updatedBy;
    }

    public void setUpdatedBy (String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [createdBy = "+createdBy+", token = "+token+", description = "+description+", specificationToken = "+specificationToken+", updatedDate = "+updatedDate+", name = "+name+", deleted = "+deleted+", createdDate = "+createdDate+", metadata = "+metadata+", namespace = "+namespace+", updatedBy = "+updatedBy+"]";
    }

}
