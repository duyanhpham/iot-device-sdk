package iotplatform.json.model.command;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import iotplatform.json.model.assignment.DeviceAssignmentType;
import iotplatform.json.model.event.DeviceEventType;

@JsonInclude(Include.NON_NULL)
public class DeviceCommandInvocation {
	private String siteToken;

	private String eventDate;
	
	private String deviceHardwareId;

	private String deviceAssignmentToken;

	private String status;

	private DeviceEventType eventType;

	private String assetModuleId;

	private String initiatorId;

	private String receivedDate;

	private String id;

	private String assetId;

	private DeviceAssignmentType assignmentType;

	private String initiator;

	private String commandToken;

	private String target;
	
	private String targetId;

	private Map<String, String> parameterValues;

	private Map<String, String> metadata;

	public String getSiteToken ()
	{
		return siteToken;
	}

	public void setSiteToken (String siteToken)
	{
		this.siteToken = siteToken;
	}

	public String getEventDate ()
	{
		return eventDate;
	}

	public void setEventDate (String eventDate)
	{
		this.eventDate = eventDate;
	}

	public String getDeviceAssignmentToken ()
	{
		return deviceAssignmentToken;
	}

	public void setDeviceAssignmentToken (String deviceAssignmentToken)
	{
		this.deviceAssignmentToken = deviceAssignmentToken;
	}

	public String getStatus ()
	{
		return status;
	}

	public void setStatus (String status)
	{
		this.status = status;
	}

	public DeviceEventType getEventType ()
	{
		return eventType;
	}

	public void setEventType (DeviceEventType eventType)
	{
		this.eventType = eventType;
	}

	public String getAssetModuleId ()
	{
		return assetModuleId;
	}

	public void setAssetModuleId (String assetModuleId)
	{
		this.assetModuleId = assetModuleId;
	}

	public String getInitiatorId ()
	{
		return initiatorId;
	}

	public void setInitiatorId (String initiatorId)
	{
		this.initiatorId = initiatorId;
	}

	public String getReceivedDate ()
	{
		return receivedDate;
	}

	public void setReceivedDate (String receivedDate)
	{
		this.receivedDate = receivedDate;
	}

	public String getId ()
	{
		return id;
	}

	public void setId (String id)
	{
		this.id = id;
	}

	public String getAssetId ()
	{
		return assetId;
	}

	public void setAssetId (String assetId)
	{
		this.assetId = assetId;
	}

	public DeviceAssignmentType getAssignmentType ()
	{
		return assignmentType;
	}

	public void setAssignmentType (DeviceAssignmentType assignmentType)
	{
		this.assignmentType = assignmentType;
	}

	public String getInitiator ()
	{
		return initiator;
	}

	public void setInitiator (String initiator)
	{
		this.initiator = initiator;
	}

	public String getCommandToken ()
	{
		return commandToken;
	}

	public void setCommandToken (String commandToken)
	{
		this.commandToken = commandToken;
	}

	public String getTarget ()
	{
		return target;
	}

	public void setTarget (String target)
	{
		this.target = target;
	}

	public Map<String, String> getParameterValues ()
	{
		return parameterValues;
	}

	public void setParameterValues (Map<String, String> parameterValues)
	{
		this.parameterValues = parameterValues;
	}

	public Map<String, String> getMetadata ()
	{
		return metadata;
	}

	public void setMetadata (Map<String, String> metadata)
	{
		this.metadata = metadata;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	public String getDeviceHardwareId() {
		return deviceHardwareId;
	}

	public void setDeviceHardwareId(String deviceHardwareId) {
		this.deviceHardwareId = deviceHardwareId;
	}


	@Override
	public String toString()
	{
		return "DeviceCommand [siteToken = "+siteToken+", eventDate = "+eventDate+", deviceAssignmentToken = "+deviceAssignmentToken+", status = "+status+", eventType = "+eventType+", assetModuleId = "+assetModuleId+", initiatorId = "+initiatorId+", receivedDate = "+receivedDate+", id = "+id+", assetId = "+assetId+", assignmentType = "+assignmentType+", initiator = "+initiator+", commandToken = "+commandToken+", target = "+target+", parameterValues = "+parameterValues+", metadata = "+metadata+"]";
	}
}
