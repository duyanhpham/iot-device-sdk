package iotplatform.json.model.message;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import iotplatform.json.model.assignment.DeviceAssignment;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RegistrationResponse {
	
	private SystemCommand systemCommand;
	
	//@JsonIgnore
	private DeviceAssignment assignment;
	
	@JsonIgnore
	private Object nestingContext;

	public enum RegistrationSuccessReason {

		/** Indicates a new device registration */
		NewRegistration,

		/** Indicates device was already registered in the system */
		AlreadyRegistered;
	}
	public enum RegistrationFailureReason {

		/** Registration manager does not allow new devices */
		NewDevicesNotAllowed,

		/** Invalid specification token was passed */
		InvalidSpecificationToken,

		/** Site token was required */
		SiteTokenRequired;
	}
	
	public SystemCommand getSystemCommand ()
	{
		return systemCommand;
	}

	public void setSystemCommand (SystemCommand systemCommand)
	{
		this.systemCommand = systemCommand;
	}

	public DeviceAssignment getAssignment ()
	{
		return assignment;
	}

	public void setAssignment (DeviceAssignment assignment)
	{
		this.assignment = assignment;
	}

	
	public Object getNestingContext() {
		return nestingContext;
	}

	public void setNestingContext(Object nestingContext) {
		this.nestingContext = nestingContext;
	}

	@Override
	public String toString()
	{
		return "RegisterResponse [systemCommand = "+systemCommand+", assignment = "+assignment+", nestingContext = "+nestingContext+"]";
	}	
}
