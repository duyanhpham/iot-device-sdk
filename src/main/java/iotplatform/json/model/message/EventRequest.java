package iotplatform.json.model.message;

/**
 * Used for marshaling device request data to JSON.
 */
public class EventRequest {

	/** Hardware id the request applies to */
	private String hardwareId;

	/** Originating invocation if available */
	//private String originator = "device";

	/** Request type */
	private RequestType type;

	/** Event create request */
	private Object request;

	public EventRequest(String hardwareId2, RequestType type, Object request) {
		this.hardwareId = hardwareId2;
		this.type = type;
		this.request = request;
	}

	public String getHardwareId() {
		return hardwareId;
	}

	public void setHardwareId(String hardwareId) {
		this.hardwareId = hardwareId;
	}

//	public String getOriginator() {
//		return originator;
//	}
//
//	public void setOriginator(String originator) {
//		this.originator = originator;
//	}

	public RequestType getType() {
		return type;
	}

	public void setType(RequestType type) {
		this.type = type;
	}

	public Object getRequest() {
		return request;
	}

	public void setRequest(Object request) {
		this.request = request;
	}

	/**
	 * Enumerates types of expected requests.
	 */
	public static enum RequestType {

		/** Register a device */
		RegisterDevice,

		/** Send a device location event */
		DeviceLocation,

		/** Send a device alert event */
		DeviceAlert,

		/** Send a device measurements event */
		DeviceMeasurements,

		/** Send a device steam create request */
		DeviceStream,

		/** Send a device stream data event */
		DeviceStreamData,

		/** Send a command acknowledgement */
		Acknowledge,

		/** Map a device to a slot on a composite device */
		//MapDevice,
	}
}