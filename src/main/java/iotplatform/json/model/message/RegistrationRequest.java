package iotplatform.json.model.message;

public class RegistrationRequest {
	String hardwareId;
	String specificationToken;
	String siteToken;
	public String getHardwareId() {
		return hardwareId;
	}
	public void setHardwareId(String hardwareId) {
		this.hardwareId = hardwareId;
	}
	public String getSpecificationToken() {
		return specificationToken;
	}
	public void setSpecificationToken(String specificationToken) {
		this.specificationToken = specificationToken;
	}
	public String getSiteToken() {
		return siteToken;
	}
	public void setSiteToken(String siteToken) {
		this.siteToken = siteToken;
	}
	public RegistrationRequest(String hardwareId, String specificationToken, String siteToken) {
		super();
		this.hardwareId = hardwareId;
		this.specificationToken = specificationToken;
		this.siteToken = siteToken;
	}
	
	
}
