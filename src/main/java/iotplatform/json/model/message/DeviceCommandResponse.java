package iotplatform.json.model.message;

import java.util.Date;

public class DeviceCommandResponse {

	public DeviceCommandResponse(String response, Date eventDate, String originatingEventId, boolean updateState) {
		super();
		this.response = response;
		this.eventDate = eventDate;
		this.originatingEventId = originatingEventId;
		this.updateState = updateState;
	}
	private String response;
	private Date eventDate;
	private String originatingEventId;
	private boolean updateState;
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DeviceCommandResponse [response=");
		builder.append(response);
		builder.append(", eventDate=");
		builder.append(eventDate);
		builder.append(", originatingEventId=");
		builder.append(originatingEventId);
		builder.append(", updateState=");
		builder.append(updateState);
		builder.append("]");
		return builder.toString();
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	public String getOriginatingEventId() {
		return originatingEventId;
	}
	public void setOriginatingEventId(String originatingEventId) {
		this.originatingEventId = originatingEventId;
	}
	public boolean isUpdateState() {
		return updateState;
	}
	public void setUpdateState(boolean updateState) {
		this.updateState = updateState;
	}
}
