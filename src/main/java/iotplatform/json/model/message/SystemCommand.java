package iotplatform.json.model.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SystemCommand
{
    private SystemCommandType type;
	private String reason;
	private String errorMessage;
	
    public enum SystemCommandType {

    	/** Acknowledges successful registration */
    	RegistrationAck,

    	/** Indicates failed registration */
    	RegistrationFailure,

    	/** Acknowledges device stream creation */
    	DeviceStreamAck,

    	/** Sends a chunk of device stream data */
    	SendDeviceStreamData,

    	/** Acknowledges response of device mapping operation */
    	DeviceMappingAck;
    }
    
    public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getReason ()
    {
        return reason;
    }

    public void setReason (String reason)
    {
        this.reason = reason;
    }

    public SystemCommandType getType ()
    {
        return type;
    }

    public void setType (SystemCommandType type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "SystemCommand [reason = "+reason+", type = "+type+"]";
    }
}