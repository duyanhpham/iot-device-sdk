package iotplatform.json.model.message;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import iotplatform.json.model.assignment.DeviceAssignment;
import iotplatform.json.model.command.DeviceCommandExecution;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DeviceCommandRequest
{
	class NestingContext {}
	
    private DeviceAssignment assignment;

    private DeviceCommandExecution command;

    @JsonIgnore
    private NestingContext nestingContext;

    public DeviceAssignment getAssignment ()
    {
        return assignment;
    }

    public void setAssignment (DeviceAssignment assignment)
    {
        this.assignment = assignment;
    }

    public DeviceCommandExecution getCommand ()
    {
        return command;
    }

    public void setCommand (DeviceCommandExecution command)
    {
        this.command = command;
    }

    public NestingContext getNestingContext ()
    {
        return nestingContext;
    }

    public void setNestingContext (NestingContext nestingContext)
    {
        this.nestingContext = nestingContext;
    }

    @Override
    public String toString()
    {
        return "DeviceCommandRequest [assignment = "+assignment+", command = "+command+", nestingContext = "+nestingContext+"]";
    }
}