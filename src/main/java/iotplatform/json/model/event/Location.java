package iotplatform.json.model.event;

import java.util.Date;

public class Location  {
	double latitude;
	double longitude;
	double elevation;
	boolean updateState = false;
	Date eventDate;

	public Location(double latitude, double longitude, double elevation, boolean updateState, Date eventDate) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.elevation = elevation;
		this.updateState = updateState;
		this.eventDate = eventDate;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getElevation() {
		return elevation;
	}
	public void setElevation(double elevation) {
		this.elevation = elevation;
	}
	public boolean isUpdateState() {
		return updateState;
	}
	public void setUpdateState(boolean updateState) {
		this.updateState = updateState;
	}
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("LocationRequest [latitude=");
		builder.append(latitude);
		builder.append(", longtitude=");
		builder.append(longitude);
		builder.append(", elevation=");
		builder.append(elevation);
		builder.append(", updateState=");
		builder.append(updateState);
		builder.append(", eventDate=");
		builder.append(eventDate);
		builder.append("]");
		return builder.toString();
	}
}
