package iotplatform.json.model.event;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Measurement {
	Map<String, Double> measurements;
	boolean updateState = false;
	Date eventDate;
	
	public Map<String, Double> getMeasurements() {
		if (measurements==null)
			measurements = new HashMap<>();
		return measurements;
	}
	public void setMeasurements(Map<String, Double> measurements) {
		this.measurements = measurements;
	}
	
	public void addMeasurement(String measurementName, Double value) {
		getMeasurements().put(measurementName, value);
	}
	
	public boolean isUpdateState() {
		return updateState;
	}
	public void setUpdateState(boolean updateState) {
		this.updateState = updateState;
	}
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	public Measurement(Map<String, Double> measurements, boolean updateState, Date eventDate) {
		super();
		this.measurements = measurements;
		this.updateState = updateState;
		this.eventDate = eventDate;
	}
	
	
}
