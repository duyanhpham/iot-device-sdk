package iotplatform.json.model.event;

public enum DeviceEventType {

	/** Group of measurements */
	Measurements,

	/** Geospatial location */
	Location,

	/** Exception condtion alert */
	Alert,

	/** Single chunk of data from a binary data stream */
	StreamData,

	/** Invocation of device command */
	CommandInvocation,

	/** Response to device command invocation */
	CommandResponse,
}
