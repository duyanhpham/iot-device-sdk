package iotplatform.json.model.event;

import java.util.Date;
import java.util.Map;

public class Alert  {
	String type;
	AlertLevel level;
	String message;
	boolean updateState = false;
	Date eventDate;
	Map<String, String> metadata;
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AlertRequest [type=");
		builder.append(type);
		builder.append(", level=");
		builder.append(level);
		builder.append(", message=");
		builder.append(message);
		builder.append(", updateState=");
		builder.append(updateState);
		builder.append(", eventDate=");
		builder.append(eventDate);
		builder.append(", metadata=");
		builder.append(metadata);
		builder.append("]");
		return builder.toString();
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public AlertLevel getLevel() {
		return level;
	}
	public void setLevel(AlertLevel level) {
		this.level = level;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isUpdateState() {
		return updateState;
	}
	public void setUpdateState(boolean updateState) {
		this.updateState = updateState;
	}
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	public Map<String, String> getMetadata() {
		return metadata;
	}
	public void setMetadata(Map<String, String> metadata) {
		this.metadata = metadata;
	}
	public Alert(String type, AlertLevel level, String message, boolean updateState, Date eventDate,
			Map<String, String> metadata) {
		super();
		this.type = type;
		this.level = level;
		this.message = message;
		this.updateState = updateState;
		this.eventDate = eventDate;
		this.metadata = metadata;
	}
	
}
