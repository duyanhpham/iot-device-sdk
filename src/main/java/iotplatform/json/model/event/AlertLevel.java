package iotplatform.json.model.event;

public enum AlertLevel {

	/** Informational alert */
	Info,

	/** Warning condition */
	Warning,

	/** Error condition */
	Error,

	/** Critical error */
	Critical;
}