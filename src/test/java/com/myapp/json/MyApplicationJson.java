package com.myapp.json;

import iotplatform.client.IotJsonClient;

/**
 * Sample application for sending device data and receiving command from IoT Platform using JSON
 * @author Pham Duy Anh
 *
 */
public class MyApplicationJson {

	public static void main(String[] args) {
		
		String mqttServerUrl = "tcp://113.171.23.142:1883";
		String mqttUsername = "myuser";
		String mqttPassword = "mypassword";
		
		String tenantId = "demo";
		String hardwareId = "device1";
		String specToken = "e5e9e29a-b1d1-48fc-9d00-f5af422d9a3f";
		
//		String mqttServerUrl = "tcp://localhost:1883";
//		String mqttUsername = "myuser";
//		String mqttPassword = "mypassword";
//		
//		String tenantId = "demo";
//		String hardwareId = "device3-hanoi";
//		String specToken = "f492ee1c-06c0-4816-b78a-9c144daaedf4";

		/* Initialise the IOT Client */
		IotJsonClient client = new IotJsonClient();

		client.setTenantId(tenantId);
		client.setHardwareId(hardwareId);
		client.setSpecToken(specToken);
		
		client.setMqttServerUrl(mqttServerUrl);
		client.setMqttUsername(mqttUsername);
		client.setMqttPassword(mqttPassword);
//		client.setServerCertificateFile(serverCertificateFile);		// if you have server certificate file. Use for TLS/SSL connection
		
		/* Register a processor to receive commands from platform */
		client.setCommandProcessor(new MyCommandProcessor());
		
		/* Start the agent */
		client.start();
	}

}
