package com.myapp.json;

import java.util.HashMap;
import java.util.Map;

import org.fusesource.mqtt.client.QoS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import iotplatform.client.json.JsonCommandProcessor;
import iotplatform.json.model.event.AlertLevel;
import iotplatform.json.model.message.DeviceCommandRequest;
import iotplatform.json.model.message.RegistrationResponse;

public class MyCommandProcessor extends JsonCommandProcessor {
	static Logger log = LoggerFactory.getLogger(MyCommandProcessor.class);

	@Override
	public void onConnected() {
		// Start by register device to server
		getMqttSender().sendRegistration();
	}
	
	@Override
	public void onReceiveDeviceCommand(DeviceCommandRequest command) {
		String cmdName = command.getCommand().getCommand().getName();
		Map<String, String> params = command.getCommand().getInvocation().getParameterValues();
		log.info("Command name: " + cmdName);
		log.info("Parameters value: " + params);

		/***********************************/
		// Execute the command logic here
		/***********************************/
		if (cmdName.equals("sayHello")) {
			String message = params.get("message");
			sayHello(message);
		}
		
		/***********************************/
		log.info("Send command ACK");		
		getMqttSender().sendCommandResponse(command.getCommand().getInvocation().getId(), 
				"Hello. Command executed!" );
	}

	@Override
	public void onRegistrationSuccess(RegistrationResponse response) {
		sendSampleData();
	}

	@Override
	public void onRegistrationFailure(RegistrationResponse response) {
		log.error("Can not register device");
		System.exit(-1);
	}
	
	
	private void sendSampleData() {
		log.info("send random data every 5 seconds");
		while (true) {
			log.info("Send some measurement data");
			Map<String, Double> measurements = new HashMap<>();
			measurements.put("temp.value", (double)getRandomInteger(11, 20));
			measurements.put("temp.humidity", 80.0);
			getMqttSender().sendMeasurement(measurements, false);
			
			log.info("Send some location");
			//getMqttSender().sendLocation("123.3", "341.2", "0", true);
			getMqttSender().sendLocation(11, -22, 33.3, true);
			
			log.info("Send some alert");
			getMqttSender().sendAlert("temp.overheat", AlertLevel.Warning, "Device is too hot", true, null);
			
			try {
				Thread.sleep(100000l);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}	
		}
	}
	
	private void sayHello(String message) {
		System.out.println("***********************");
		System.out.println(message);
		System.out.println("***********************");
	}
	
	/**
	 * Generate a random integer in a given range
	 */
	public static int getRandomInteger(int min, int max) {
		return min + (int)(Math.random() * ((max - min) + 1));
	}
}
