package com.myapp.protobuf;

import iotplatform.client.IotProtobufClient;

/**
 * Sample application for sending data and receiving command from IoT Platform 
 * messages are encoded using Google Protobuf 2.5
 * @author Pham Duy Anh
 *
 */
public class MyApplicationProto {

	public static void main(String[] args) {
		
		String mqttServerUrl = "tcp://localhost:1883";
		
		String mqttUsername = "myuser";
		String mqttPassword = "mypassword";
		
		String hardwareId = "nhietke1";
		String specToken = "d2604433-e4eb-419b-97c7-88efe9b2cd41";

		String tenantId = "prototenant";
		
//		String hardwareId = "123-45464b";
//		String specToken = "7dfd6d63-5e8d-4380-be04-fc5c73801dfb";
		
		/* Initialise the IOT Client */
		IotProtobufClient client = new IotProtobufClient();
		
		client.setMqttServerUrl(mqttServerUrl);
		client.setMqttUsername(mqttUsername);
		client.setMqttPassword(mqttPassword);
		
		client.setHardwareId(hardwareId);
		client.setSpecToken(specToken);
		
		client.setTenantId(tenantId);
		
		/* Register a processor to receive commands from platform */
		client.setCommandProcessor(new MyCommandProcessorProto());
		
		/* Start the agent */
		client.start();
	}

}
