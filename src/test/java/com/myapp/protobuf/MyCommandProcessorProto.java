package com.myapp.protobuf;

import iotplatform.protobuf.proto.Iot.Device.RegistrationAck;
import iotplatform.protobuf.proto.Iot.Model.AlertLevel;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.myapp.protobuf.MySpecSample.SampleSpec;

import iotplatform.client.protobuf.ProtobufCommandProcessor;

public class MyCommandProcessorProto extends ProtobufCommandProcessor {

	@Override
	public void onReceiveDeviceCommand(byte[] data) {
		try {

			ByteArrayInputStream stream = new ByteArrayInputStream(data);

			// Decode header to see which command is this
			SampleSpec._Header header = SampleSpec._Header.parseDelimitedFrom(stream);

			System.out.println(header.getOriginator()); // This is command invocation id !!!
			
			switch (header.getCommand()) {
			case PING:		
				System.out.println("PING");
				
				// Send command response
				getMqttSender().sendCommandResponse(header.getOriginator(), "Yes! I pinged!");
				
				break;
			case CHANGEBACKGROUND:	
				// Decode request and execute command
				SampleSpec.changeBackground cmd = SampleSpec.changeBackground.parseDelimitedFrom(stream);
				String color = cmd.getColor();
				System.out.println("Change background color to " + color);
				
				// Send command response
				getMqttSender().sendCommandResponse(header.getOriginator(), "Yes! I changed my background");
				
				break;
			default:
				break;
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error("Can not parse command");
			e.printStackTrace();
		}
		
	}

	@Override
	public void onRegistrationSuccess(RegistrationAck ack) {
		//getMqttSender().sendAlert("test", "info", "khong van de", true, null);
		try {
			// Send a simple measurement value
			getMqttSender().sendMeasurement("temp", 44.0);
			
			// Send device location
			getMqttSender().sendLocation(10.2, 20.3, -34.4);
			
			// Send a list of measurement value
			Map<String, Double> m = new HashMap<>();
			m.put("temp1", 20.3);
			m.put("temp2", 30.4);
			getMqttSender().sendMeasurement(m);
			
			// Send a simple alert
			getMqttSender().sendAlert("testAlert", AlertLevel.Info, "hello", null);
			
			// Send an alert with additional metadata
			Map<String, String> metadata = new HashMap<String, String>();
			metadata.put("errorPart", "part1");
			metadata.put("partNo", "abc1234");
			getMqttSender().sendAlert("testAlert2", AlertLevel.Error, "big error", metadata);
			
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Exception when sending MQTT message to server");
			// Handle failed logic here
		}
	}

	@Override
	public void onRegistrationFailure(RegistrationAck ack) {
		System.out.println("Can not register to server");
		System.exit(-1);
	}

}
